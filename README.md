# README #
### Quick summary
Implements basic logger for multithreaded environment.
Also contains dirty usage example in main.cpp

### How do I get set up? ###
Use C++ 11, link with pthread if on Linux.
Install doxygen if you want to use documentation.

### Who do I talk to? ###
Skype: vladimir.tsyshnatiy; 
Email: vtsyshnatiy@gmail.com