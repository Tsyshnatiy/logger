/*! 
 * \author Vladimir Tsyshnatiy
 * \copyright does not exist
 * \file logger.h
 */

#pragma once

#include <stdarg.h>	// variadics

/*!
 * \def LOG_DEBUG(fmt, ...)
 *	Used for logging debug messages. Does nothing in debug mode
 * \def LOG_INFO(fmt, ...)
 * Computes the maximum of \a x and \a y.
 * \def LOG_ERROR(fmt, ...)
 * Computes the maximum of \a x and \a y.
*/
#define LOG_DEBUG(fmt, ...) \
            Logger::log(fmt, "DEBUG", ##__VA_ARGS__);
            
#define LOG_INFO(fmt, ...) \
            Logger::log(fmt, "INFO", ##__VA_ARGS__);
            
#define LOG_ERROR(fmt, ...) \
            Logger::log(fmt, "ERROR", ##__VA_ARGS__);

/*! 
 * \brief Logger modes enumeration
 * \details 
 *  Provide LoggerMode by calling Logger::setMode to change logger mode
 * \copyright does not exist
 */
typedef enum
{
    STANDARD,
    DEBUG,
} LoggerMode;

/*! 
 * \brief Logger standart implementation
 * \author Vladimir Tsyshnatiy
 * \details 
 * 	Logger for multithreaded environment and formatted input in printf style.
 * 	Do not call Logger::log directly, use macroses instead.
 * \copyright does not exist
 */
class Logger final
{
private:
	// Forbid any king of constructors and related operators
    Logger() = delete;
    
    Logger(const Logger&) = delete;
    Logger& operator = (const Logger&) {};
    
    Logger(const Logger&&) = delete;
    Logger& operator = (const Logger&&) {};
    
    // Current logger mode
    static LoggerMode mode;
public:

	/*! 
	 * Sets logging mode 
	 * Arguments:
	 * 	newMode: new logger mode
	 * Returns:
	 *  Nothing
	 */
    static void setMode(const LoggerMode newMode) { mode = newMode; }
    
	/*! 
	 * Gets logging mode 
	 * Arguments:
	 * 	None
	 * Returns:
	 *  Current logger mode
	 */
    static LoggerMode getMode() { return mode; }
    
    // Variadic log function for formatted logging
    /*! 
	 * Variadic log function for formatted logging
	 * Arguments:
	 * 	fmt: Format string in printf terms
	 *  type: DEBUG, INFO or ERROR.
	 *  Variadic args: same as in printf
	 * Returns:
	 *  Current logger mode
	 */
    static void log(const char* fmt, const char* type...);
};
