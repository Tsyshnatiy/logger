#include <iostream>
#include <future>
#include <thread>

#include "logger.h"

using namespace std;

bool a()
{
	LOG_DEBUG("A");
	return false;
}

bool b()
{
	LOG_DEBUG("B");
	return false;
}

int main()
{
	auto ftr1 = async(std::launch::async, a);
	auto ftr2 = async(std::launch::async, b);
	
	LOG_INFO("%s", "merge threads");
	
	ftr1.get();
	ftr2.get();
	
	LOG_INFO("%s", "finished");
	
	return 0;
}
