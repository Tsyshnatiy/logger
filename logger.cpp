#include <stdio.h> 	// printf, vprintf, etc
#include <stdint.h>	// uint64_t
#include <thread>	// current_thread
#include <string>	// std string

#include "logger.h"

// default logger mode
LoggerMode Logger::mode = DEBUG;

// this func logs in format
// [<thread_id>]<message_type><message>\n
void Logger::log(const char* fmt, const char* type...)
{
	// Check if we are in debig mode.
	// If not, return immediately
    if (mode != DEBUG && std::string("DEBUG") == type)
    {
        return;
    }
	auto id = std::this_thread::get_id();
	
	// Print out everything in declared format
	// Print out thread id
    printf("[TH:");
    printf("%x", *((unsigned*)&id));
    printf("]");
    
    // Print out type
    printf("%3s: ", type);
    
    // Print out formatted message
    va_list args;
    va_start(args, type);
    vprintf(fmt, args);
    fflush(stdout);
    va_end(args);
    printf("\n");
}
